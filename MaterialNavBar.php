<?php

namespace faboslav\materialnavbar;

use yii\base\InvalidConfigException;
use yii\bootstrap\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class MaterialNavBar extends Widget
{
    public $items = [];

    /**
     * @var boolean whether to enable ripple effect on the menu items.
     */
    public $ripple = true;

    /**
     * @inheritdoc
     */
    public function run() {
        $this->initWidget();
        echo $this->renderItems();
    }

    /**
     * Initializes the widget settings.
     */
    public function initWidget()
    {
        Html::addCssClass($this->options, ['widget' => 'material-navbar']);

        $this->registerAssets();
    }

    /**
     * Registers the assets.
     */
    public function registerAssets()
    {
        $view = $this->getView();
        MaterialNavBarAsset::register($view);
        $id = 'jQuery("#' . $this->options['id'] . '")';
        $view->registerJs("{$id}.materialNavBar();");

        if($this->ripple) {
            $selector = 'jQuery(".'.$this->options['class']['widget'].' li a")';
            $view->registerJs("{$selector}.materialRipple();");
        }
    }

    /**
     * Renders navbar items
     *
     * @return string the rendering result.
     */
    public function renderItems()
    {
        $items = [];
        foreach ($this->items as $i => $item) {
            $items[] = $this->renderItem($item);
        }

        $list = Html::tag('ul', implode("\n", $items));

        return Html::tag('nav', $list, $this->options);
    }

    /**
     * @inheritdoc
     */
    public function renderItem($item)
    {
        if (is_string($item)) {
            return $item;
        }

        if (!isset($item['label'])) {
            throw new InvalidConfigException("The 'label' option is required.");
        }

        $options = ArrayHelper::getValue($item, 'options', []);
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);

        $url = ArrayHelper::getValue($item, 'url', '#');

        if (isset($item['isActive']) && $item['isActive']) {
            Html::addCssClass($options, 'active', $linkOptions);
        }

        return Html::tag('li', Html::a($item['label'], $url), $options);
    }
}
