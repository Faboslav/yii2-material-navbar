(function ( $ ) {
    $.fn.materialNavBar = function() {

        var init = function() {
            $('.material-navbar').append('<div class="material-navbar-border"></div>');

            activeItem = $('.material-navbar ul').find("li.active");

            if(activeItem.length) {
                setActiveItem(activeItem);
            }
        };

        var setActiveItem = function(activeItem) {
            $('.material-navbar ul li.active').removeClass('active');

            if(!activeItem.hasClass('active')) {
                activeItem.addClass('active');
            }

            setBorder(activeItem);
        };

        var setBorder = function(activeItem) {
            width = activeItem.width();
            left = $('a', activeItem).offset().left - $('.material-navbar').offset().left;

            $('.material-navbar .material-navbar-border').css({
                'left' : left+'px',
                'width' : width+'px'
            });
        };

        $(document).on('click', '.material-navbar ul li', function() {
            setActiveItem($(this));
        });

        $( window ).resize(function() {
            activeItem = $('.material-navbar ul li.active');

            if(activeItem.length) {
                setBorder(activeItem);
            }
        });

        $(document).on('ready', function() {
            init();
        });

        return this;
    };
}( jQuery ));

$.fn.materialRipple = function(options) {
    var defaults = {
        rippleClass: 'ripple-wrapper'
    };

    $.extend(defaults, options);

    $('body').on('animationend webkitAnimationEnd oAnimationEnd', '.' + defaults.rippleClass, function () {
        removeRippleElement(this);
    });

    var addRippleElement = function(element, e) {
        $(element).append('<span class="added '+defaults.rippleClass+'"></span>');
        newRippleElement = $(element).find('.added');
        newRippleElement.removeClass('added');

        // get Mouse Position
        var mouseX = e.pageX;
        var mouseY = e.pageY;

        // for each ripple element, set sizes
        elementWidth = $(element).outerWidth();
        elementHeight = $(element).outerHeight();
        d = Math.max(elementWidth, elementHeight);
        newRippleElement.css({'width': d, 'height': d});

        var rippleX = e.clientX - $(element).offset().left - d/2 + $(window).scrollLeft();
        var rippleY = e.clientY - $(element).offset().top - d/2 + $(window).scrollTop();

        // Position the Ripple Element
        newRippleElement.css('top', rippleY+'px').css('left', rippleX+'px').addClass('animated');
    };

    var removeRippleElement = function($element) {
        $element.remove();
    };

    // add Ripple-Wrapper to all Elements
    $(this).addClass('ripple');

    // Let it ripple on click
    $(this).bind('click', function(e){
        addRippleElement(this, e);
    });
}