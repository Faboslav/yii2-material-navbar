## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
$ php composer.phar require faboslav/yii2-material-navbar "dev-master"
```

or add

```
"faboslav/yii2-material-navbar": "dev-master"
```

to the ```require``` section of your `composer.json` file.

## Usage

### MaterialNavBar

```php
use faboslav\materialnavbar\MaterialNavBar

echo MaterialNavBar::widget([
    'items' => [
        ['label' => 'Page one', 'url' => '#'],
        ['label' => 'Page two', 'url' => '#', 'isActive' => true],
        ['label' => 'Page three', 'url' => '#'],
        ['label' => 'Page four', 'url' => '#'],
        ['label' => 'Page five', 'url' => '#'],
    ]
]);
```
## Licence

MIT