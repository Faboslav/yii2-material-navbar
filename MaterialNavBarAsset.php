<?php

namespace faboslav\materialnavbar;

class MaterialNavBarAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@vendor/faboslav/yii2-material-navbar/assets';

    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];

    public $css = [
        'css/material-navbar.css',
    ];
    public $js = [
        'js/material-navbar.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
